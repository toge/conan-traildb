project(traildb C)

cmake_minimum_required(VERSION 3.0)

include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()

file(GLOB_RECURSE tdb_src src/*.c)
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -DDSFMT_MEXP=521")

include_directories(src)

add_library(traildb STATIC ${tdb_src})
