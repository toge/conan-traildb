#include <iostream>

extern "C" {
#include "traildb.h"
}

int main() {
   auto cons = tdb_cons_init();
   tdb_cons_close(cons);

   return 0;
}
