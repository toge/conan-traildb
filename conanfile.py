from conans import ConanFile, CMake, tools
import shutil

class TraildbConan(ConanFile):
    name        = "traildb"
    version     = "0.6"
    license     = "MIT"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-traildb/"
    description = "TrailDB is an efficient tool for storing and querying series of events http://traildb.io"
    topics      = ("series of events", "query")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    requires    = ["libjudy/[>= 1.0.5]@toge/stable"]
    exports     = "CMakeLists.txt"

    def source(self):
        tools.get("https://github.com/traildb/traildb/archive/{}.zip".format(self.version))
        shutil.move("traildb-{}".format(self.version), "traildb")
        shutil.copy("CMakeLists.txt", "traildb/CMakeLists.txt")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="traildb")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="traildb/src")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["judy", "traildb"]
